<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;


// Article Controller
Route::post('/article','ArticleController@store');
Route::get('/article','ArticleController@create');
Route::get('/articles/{id}','ArticleController@userArticles');


// Likes For Article Controller
Route::get('/articles/{id}/like/toggle','ArticleController@toggleLike');


// Contact Controller
// Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');


Route::get('/contact', 'ContactController@show');
Route::post('/contact',  'ContactController@mailToAdmin');


// Comment Controller
Route::get('/comment', 'CommentController@create');
Route::post('/comment', 'CommentController@store');


// Index Controller
Route::get('/', 'IndexController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
