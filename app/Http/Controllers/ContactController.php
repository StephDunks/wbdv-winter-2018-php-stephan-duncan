<?php

namespace App\Http\Controllers;
use App\Notifications\InboxMessage;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Http\Requests\ContactFormRequest;
use App\Admin;

class ContactController extends Controller
{
    //  public function create() {
    //
    //
    //     return view('contact');
    // }
    //
    //  public function store() {
    //
    //  	$request = request();
    //
		//   $this->validate($request,[
    //  		'name' => 'required|max:255',
    //  		'email' => 'required|email|unique:users,email',
    //  		'comments' => 'required'
    //       ],
    //  		[
		// 	 'name.required' => 'Please Enter A Name',
		// 	 'email.required' => 'Please Enter An Email',
		// 	 'comments.required' => 'Please write a short message'
    //
    //  		]);
    //

    //  	  $data = request()->all();
    //
    //     $contact = new Contact();
    //     $contact->name = $data['name'];
    //     $contact->email = $data['email'];
    //     $contact->content = $data['comments'];
    //     $contact->save();
    //
    //
    //     return redirect ('/contact')->with('message', 'Your message was successfully processed. Someone will be in touch with you soon!');
    // }



        public function show()
    	{
    		return view('contact');
    	}

    	public function mailToAdmin(ContactFormRequest $message, Admin $admin)
    	{        //send the admin an notification
    		$admin->notify(new InboxMessage($message));
    		// redirect the user back
    		return redirect()->back()->with('flash_message', 'thanks for the message! We will get back to you soon!');
    	}


}
