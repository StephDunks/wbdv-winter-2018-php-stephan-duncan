<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Article;
Use App\User;

class CommentController extends Controller
{
  public function create() {

    if(!\Auth::check()) {
        return redirect('/login');
      }

    return view('comment');

  }


    public function store() {

      if(!\Auth::check()) {
      return redirect('/login');
    }

      $request = request();
      $loggedInUser = $request->user();
      $data = request()->all();


        $this->validate($request,[
           'content' => 'required|max:255'],

            [
             'content.required' => 'Please Write Comment'
           ]);
     //
     //  $result = $request->validate (


       $comment = new Comment();
       $comment->user_id =  $loggedInUser->id;
       $comment->article_id = $data['article_id'];
       $comment->content = $data['content'];
       $comment->save();

       return redirect ('/');

    }
}
