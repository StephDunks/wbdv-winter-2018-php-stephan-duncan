<?php

namespace App\Http\Controllers;

use Faker\Factory;
use Illuminate\Http\Request;
use App\Models\Comments;
use App\Models\Post;
use App\User;


class PostController extends Controller
{
     public function create() {

       if(!\Auth::check()) {
         return redirect('/login');
       }


        return view('/article');
    }

     public function store() {

       if(!\Auth::check()) {
         return redirect('/login');
       }

       $request = request();
       $loggedInUser = $request->user();
       $data = request()->all();



     	 $result = $request->validate ([
     		'content' => 'required|max:255'],

     		[
			 'content.required' => 'Please Post A Comment'
			]);

         $post = new Post();
         $post->user_id = $loggedInUser->id;
         $post->content = $data['content'];
         $post->save();
         return redirect ('/')->with('message', 'Your comment was successfully posted.');
    }

      public function userPosts($userId) {

        $user = User::where('id', $userId)
            ->first();

        if (!$user) {
            abort(404);
        }

        $posts = $user->posts;

        return view('userPosts', [
            'user' => $user,
            'posts' => $posts
        ]);

      }

      public function toggleLike($postId)
      {
        $user = request()->user();
        $post = Post::find($postId);

        if ($post->isLikedByCurrentUser()) {
            $post->likes()->detach($user);
        } else {
            $post->likes()->attach($user);
        }

        return back()
          ->with('message', 'You liked a post!');
      }

 }
