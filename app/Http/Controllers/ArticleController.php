<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\User;
use Validator;


class ArticleController extends Controller
{
    public function create() {

      if(!\Auth::check()) {
         return redirect('/login');
       }

      return view('article');
    }

    public function store() {
      //
      $request = request();

      $this->validate($request, [
     		'category' => 'required|max:255',
     		'title' => 'required|max:255',
        'description' => 'required|max:255',
     		'content' => 'required'
          ],
     		[
       'category.required' => 'please enter a category',
			 'author.required' => 'Please Enter A Name',
			 'title.required' => 'Please Enter A Title',
			 'description.required' => 'Please write a description',
       'content.required' => 'Please write in some content'
     		]);

      $loggedInUser = $request->user();
      $data = request()->all();


      $article = new Article();
      $article->user_id= $loggedInUser->id;
      $article->category = $data['category'];
      $article->title = $data['title'];
      $article->description = $data['description'];
      $article->content = $data['content'];
      $article->save();

      return redirect('/')->with('message', 'Your article was successfully posted.');
    }

    public function userArticles($userId) {

      $user = User::where('name', $userId)
          ->orWhere('id', $userId)
          ->first();

      if (!$user) {
          abort(404);
      }

      $articles = $user->article;

      return view('userArticles', [
          'user' => $user,
          'articles' => $articles
      ]);

    }

    public function toggleLike($articleId)
    {
      $user = request()->user();
      $article = Article::find($articleId);

      if ($article->isLikedByCurrentUser()) {
          $article->likes()->detach($user);
      } else {
          $article->likes()->attach($user);
      }

      return back()
        ->with('message', 'You liked this article!');
    }
}
