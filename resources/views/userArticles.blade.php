@extends('layout')


@section('content')
   <div class="container">
        <div class="row">

          <div class="col-md-12">
              @foreach($articles as $article)
                <div class="card card1">
                    <ul class="card-description">
                         <li>
                            <img class="article-image"  src="<?php echo $article->image?>"><br>
                            <span class="article-author"><?php echo $article->user->name?></span><br>
                            <span class="article-category"><?php echo $article->category?></span><br>
                            <span class="article-title"><?php echo $article->title?></span><br>
                            <span class="article-description"><?php echo $article->description?></span><br>
                            <span class="article-content"><?php echo $article->content?></span><br>
                        </li>
                     </ul>
                 </div>

                 {{-- Outputs Comments To The Homepage  --}}
                 <div>
                   <?php foreach ($article->comments as $comment): ?>
                     <strong>Comment:</strong><?php echo $comment->content ?><br>
                     <strong>User:</strong><?php echo $comment->user->name?><br>
                     <strong>Date:</strong><?php echo $comment->updated_at->diffForHumans()?><br><br>
                   <?php endforeach; ?>
                 </div>

                 {{-- Outputs Likes To The Homepage  --}}
                 <span class="like-count"><?php echo count($article->likes) ?></span>
                    @if ($article->isLikedByCurrentUser())
                      <a href="/articles/{{$article->id}}/like/toggle"><i class="far fa-heart"></i><span class="like">Unlike<span></a>
                    @else
                      <a href="/articles/{{$article->id}}/like/toggle"><i class="far fa-heart"></i><span class="like">Like<span></a>
                    @endif

                  {{-- Comment Box  --}}
                 <form method="post" action="/comment">
                 <?php echo csrf_field() ?>
                   <div class="comment-input">
                     <textarea rows="3" cols="60" name = "content"></textarea><br>
                     <input type="submit" value="comment" class="comment-button"><br>
                     <input type="hidden" name="article_id" value="{{$article->id}}">
                   </div><br>
                 </form>
               @endforeach
           </div>

        </div>
    </div>



   @endsection
