@extends('layout')


@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-4">
                  <div class="card card1">
                      <ul class="card-description">
                           <li>
                              <a href="https://placeholder.com"><img src="http://via.placeholder.com/350x150" class="article-image1"></a><br>
                              <span class="article-category">Business</span><br>
                              <span class="article-description">Lorem ipsum dolor sit amet</span><br>
                              <span class="article-author">Josh King</span><br>
                          </li>
                       </ul>
                   </div>
              </div>
              <div class="col-md-4">
                    <div class="card card1">
                        <ul class="card-description">
                             <li>
                                <a href="https://placeholder.com"><img src="http://via.placeholder.com/350x150" class="article-image1"></a><br>
                                <span class="article-category">Culture</span><br>
                                <span class="article-description">Lorem ipsum dolor sit amet</span><br>
                                <span class="article-author">James Walker</span><br>
                            </li>
                         </ul>
                     </div>
                </div>
                <div class="col-md-4">
                      <div class="card card1">
                          <ul class="card-description">
                               <li>
                                  <a href="https://placeholder.com"><img src="http://via.placeholder.com/350x150" class="article-image1"></a><br>
                                  <span class="article-category">Gear</span><br>
                                  <span class="article-description">Lorem ipsum dolor sit amet</span><br>
                                  <span class="article-author">Farrah Newport</span><br>
                              </li>
                           </ul>
                       </div>
                  </div>
              </div>
    </div>
          @if (Auth::check())
          <h1 class="user-article">Your Articles </h1>
          @endif
            <div class="row">
                  <div class="col-md-12">
                     <?php foreach($articles as $article): ?>
                        <div class="card container card1">
                            <ul class="card-description">
                                 <li>
                                    <img class="article-image"  src="<?php echo $article->image?>"><br>
                                    <span class="article-category"><?php echo $article->category?></span><br>
                                    <span class="article-title"><?php echo $article->title?></span><br>
                                    <span class="article-description"><?php echo $article->description?></span><br>
                                    <span class="article-author"><a href="/articles/<?php echo $article->user->name?>"><?php echo $article->user->name?></a></span><br>
                                </li>
                             </ul>
                         </div>
                       <?php endforeach ?>
                  </div>
             </div>





   @endsection
