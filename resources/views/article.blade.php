@extends('layout')

@section('content')


    <form method="post">
    <?php echo csrf_field()?>
      <div class="create-post">
        <h1>Create An Article</h1>
        <div>
          <label for="title">Title:</label><br>
          <input type="text" name="title" value="{{ old('title') }}">
          @if ($errors->has('title'))
          <span class="help-block">
              <strong>{{ $errors->first('title') }}</strong>
          </span>
          @endif
        </div>
        <div>
          <label for="description">Description:</label><br>
          <input type="text" name="description" value="{{ old('description') }}">
          @if ($errors->has('description'))
          <span class="help-block">
              <strong>{{ $errors->first('description') }}</strong>
          </span>
          @endif
        </div>
        <div>
          <label for="category">Category:</label><br>
          <input type="text" name="category" value="{{ old('category') }}">
          @if ($errors->has('category'))
          <span class="help-block">
              <strong>{{ $errors->first('category') }}</strong>
          </span>
          @endif
        </div>
        <div>
        </div>
        <div>
          <label for="content">Content:</label><br>
          <textarea rows="5" cols="60" name="content" value="{{ old('content') }}"></textarea>
          @if ($errors->has('content'))
          <span class="help-block">
              <strong>{{ $errors->first('content') }}</strong>
          </span>
          @endif
        </div>
        <input type="submit" value="Post Article" class="post-button">
      </div>
    </form>
@endsection
