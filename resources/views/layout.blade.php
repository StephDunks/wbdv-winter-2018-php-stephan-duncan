<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
        <title>Wired</title>

        <!-- Fonts -->
       <!--  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    </head>
    <body>
      <header>
      <div class="nav">
      <div>
       <ul class="primary-navigation" role="navigation">
           <li class="menu"><img src="hamburgermenu.png"></li>
           <li class="logo"><a href="/"><img src="wiredlogo.png"></a></li>
            @if (Auth::check())
           <li class="createarticle"><a href="/article">Create Article</a></li>
            @endif
           <li class="signin">
             @if (Auth::check())
               Hello, {{Auth::user()->name}}
               <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                   Logout
               </a>

               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
             @else
               <a href="/login">Sign In</a>
             @endif
           </li>
           <li class="subscribe"><a href="/register">Subscribe</a></li>
           <li class="subscribe"><a href="/contact">Contact</a></li>
           <li class="search"><img src="search.png"></li>
          </
        </ul>
       </div>
        <div>
        <ul class="secondary-navigation" role="navigation">
            <li class="business"><a href="">Business</a></li>
            <li class="culture"><a href="">Culture</a></li>
            <li class="gear"><a href="">Gear</a></li>
            <li class="ideas"> <a href="">Ideas</a></li>
            <li class="science"><a href="">Science</a></li>
            <li class="security"><a href="">Security</a></li>
            <li class="transportation"><a href="">Transportation</a></li>
         </ul>
         <div>
       </div>
      </header>



        @yield('content')

        <footer>
          <div class="footer-section">
            <ul class="footer-links">
            <li class="footer-logo"><a href="/"><img src="wiredlogo.png"></a></li>
            <li>
            <ul class="social-icons">
              <li><i class="fab fa-facebook-square"></i></li>
              <li><i class="fab fa-twitter-square"></i></li>
              <li><i class="fab fa-pinterest-square"></i></li>
              <li><i class="fab fa-youtube-square"></i></li>
              <li><i class="fab fa-instagram"></i></li>
            </ul>
            <li>
            <ul>
            </div>
       </footer>
    </body>

</html>
