<!-- Name & Email Field -->
<div class="form-group col-md-12 <?php echo $errors->has($name) ? 'has-error' : ''?>">
    <label for="{{ $name }}">{{ $label }}</label>
    <input
        type="text"
        name="{{ $name }}"
        placeholder="{{ $label }}"
        value="{{ old($name) }}"
        class="form-control {{ $errors->has($name) ? 'is-invalid' : '' }}"
        id="focusedInput" />
    @if($errors->has($name))
        <span class="invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>
