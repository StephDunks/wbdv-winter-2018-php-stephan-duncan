<!-- Comments Field -->
<div class="form-group error col-md-12 <?php echo $errors->has('comments') ? 'has-error' : ''?>">
  <label for="comments">Message</label><br>
  <textarea name="comments" placeholder="Please write a brief message" rows = "6" cols="30" class="form-control <?php echo old('comments') ? 'is-invalid' : ''?>"></textarea>
  <?php if($errors->has('comments')): ?>
    <span class="invalid-feedback"><?php echo $errors->first('comments') ?>
  <?php endif; ?>
</div>
